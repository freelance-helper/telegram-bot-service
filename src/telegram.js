const Telegram = require('telegraf/telegram');
const {config} = require('./config');

const telegram = new Telegram(config.telegramTokenBot);

exports.telegram = telegram;
