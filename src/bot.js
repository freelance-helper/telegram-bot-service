require('dotenv').config();
const Telegraf = require('telegraf');
const Stage = require('telegraf/stage');
const session = require('telegraf/session');
const Extra = require('telegraf/extra');

const {config} = require('./config');
const startScene = require('./controllers/start');
const authnenticateScene = require('./controllers/autnenticate');

const stage = new Stage([
    startScene,
    authnenticateScene
]);
const bot = new Telegraf(config.telegramTokenBot);
bot.use(session());
bot.use(stage.middleware());

bot.command('/authenticate', async ctx => {
    return ctx.scene.enter('authenticate');
});

bot.start(async ctx => ctx.scene.enter('start'));

module.exports = bot;
