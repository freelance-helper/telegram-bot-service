exports.config = {
    telegramTokenBot: process.env.TELEGRAM_BOT_TOKEN,
    adminServiceHost: process.env.ADMIN_SERVICE_HOST,
    botServicePort: process.env.BOT_SERVICE_PORT,
};
