const express = require('express');
const bodyParser = require('body-parser');
const Extra = require('telegraf/extra');

const bot = require('./bot');
const {config} = require('./config');

const app = express();

app
    .use(bodyParser.json())
    .post('/message/:id', async (req, res) => {
        console.log('new message', req.body);
        console.log(req.params);

        bot.telegram.sendMessage(req.params.id, req.body.message, Extra.HTML());

        res.send('message')
    })
    .listen(config.botServicePort, () => {
        console.log(`Bot backend starting in http://localhost:${config.botServicePort}`)
    });

bot.startPolling();
console.log('Bot starting');
