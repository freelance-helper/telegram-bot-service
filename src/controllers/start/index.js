const Stage = require('telegraf/stage');
const Scene = require('telegraf/scenes/base');

const {getStartKeyboard} = require('./keyboards');

const start = new Scene('start');

start.enter(async ctx => {
    const uid = ctx.from.id;

    console.log(ctx.message.chat.id);

    await ctx.reply('Hello in Freelancer Assistant Bot', getStartKeyboard());
});

module.exports = start;
