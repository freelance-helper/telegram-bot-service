const { Extra, Markup } = require('telegraf');

exports.getStartKeyboard = function () {
    return Extra.HTML().markup((m) => (
        m.inlineKeyboard([
            m.callbackButton('Авторизоваться', JSON.stringify({a: 'authenticate'}))
        ])
    ));
};
