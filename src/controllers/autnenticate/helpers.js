const jimp = require('jimp');
const QrCode = require('qrcode-reader');

exports.getTokenFromQR = async function getTokenFromQR(imageLink) {
    const img = await jimp.read(imageLink);

    const qr = new QrCode();
    const value = await new Promise((resolve, reject) => {
        qr.callback = (err, v) => err != null ? reject(err) : resolve(v);
        qr.decode(img.bitmap);
    });

    return JSON.parse(value.result).token;
};

exports.getPhotoIdFromMessage = function getPhotoIdFromMessage(ctx) {
    return ctx.update.message.photo[ctx.update.message.photo.length - 1].file_id;
};
