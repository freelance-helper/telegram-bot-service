const Scene = require('telegraf/scenes/base');
const {axios} = require('../../axios');

const {telegram} = require('../../telegram');
const {config} = require('../../config');
const {getTokenFromQR, getPhotoIdFromMessage} = require('./helpers');

const authenticate = new Scene('authenticate');

authenticate.enter(async ctx => {
    await ctx.reply('Отправте фото с QR кодом мне, код можно взять в приложении');
});
authenticate.on('photo', async ctx => {
    const fileId = getPhotoIdFromMessage(ctx);
    const fileLink = await telegram.getFileLink(fileId);
    let token;

    try {
        token = await getTokenFromQR(fileLink);
    } catch (e) {
        await ctx.reply('Попробуй отправить еще одно фото');
    }

    try {
        const res = await axios.post('/authenticate/telegram', {
            tid: ctx.from.id
        }, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        ctx.session = {
            userId: res.data.userId
        };

        await ctx.reply('Отлично');
        ctx.scene.leave();
        
    } catch (e) {
        console.log(e);
        await ctx.reply('Пользователь не найдет');
    }
});

module.exports = authenticate;
