const a = require('axios');
const {config} = require('./config');

const axios = a.create({
    baseURL: config.adminServiceHost
});

exports.axios = axios;
